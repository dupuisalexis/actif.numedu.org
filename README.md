# Playground Actif

## Description
Cette page web propose un formulaire d'assistance à la structuration de prompts à utiliser dans les principaux agents conversationnels. Il respecte le modèle de prompt "ACTIF" qui vise à obtenir rapidement une réponse adaptée au besoin initial.

![](https://prompting.numedu.org/assets/ACTIF_2.svg)

## Installation
Cette page web exploite uniquement des fichiers ressources externes (images,javascripts,css). Il suffit donc d'héberger le fichier index.html et de le consulter via un navigateur web quelconque.

## Usage
L'usage s'effectue en trois étapes :

- saisir les éléments qui composent le prompt puis valider
- copier le résultat
- ouvrir son agent conversationnel préféré et coller le texte dans la zone de saisie du prompt

## Tester
Rendez-vous sur : 
* La forge : https://actif-numedu-org-dupuisalexis-2c30fa4aed7eefe57800a026025fb44ba.forge.apps.education.fr/
* numedu.org : https://actif.numedu.org

## Bonus
Une banque de prompts orientés "éducation - formation" : 
https://prompting.numedu.org